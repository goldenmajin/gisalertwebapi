﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entity.Model.MediaManagement;

public class Media
{
    public Media(string filePath)
    {
        FilePath = filePath;
    }

    public int Id { get; set; }

    [Required]
    public string FilePath { get; set; }
}