﻿namespace Domain.Entity.Model.UserManagement
{
    public class UserStatistic
    {
        public UserStatistic(string role)
        {
            Role = role;
        }
        public string Role { get; set; }
        public int Count { get; set; }
    }
}
