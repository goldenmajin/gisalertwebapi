﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entity.Model.EventManagement;

public class EventTypeColor
{
    public EventTypeColor(string name, string hexCode)
    {
        Name = name;
        HexCode = hexCode;
    }

    public EventTypeColor(){}

    public int Id { get; set; }

    [Required]
    [MaxLength(20)]
    public string Name { get; set; }

    [Required]
    [MaxLength(6)]
    public string HexCode { get; set; }
}