﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Model.MediaManagement;

namespace Domain.Entity.Model.EventManagement;

public class Event
{
    public Event(){}
    public Event(string title, decimal latitude, decimal longitude, int radius, string description, EventType type,
        DateTime dateFrom,
        DateTime dateTo)
    {
        Title = title;
        Latitude = latitude;
        Longitude = longitude;
        Radius = radius;
        Description = description;
        Type = type;
        DateFrom = dateFrom;
        DateTo = dateTo;
        Active = true;
        CreatedOn = DateTime.Now;
    }

    public int Id { get; set; }

    [Required] 
    [MinLength(1)]
    [MaxLength(30)]
    public string Title { get; set; }

    [Required]
    [Column(TypeName = "decimal(10,7)")]
    public decimal Latitude { get; set; }

    [Required]
    [Column(TypeName = "decimal(10,7)")]
    public decimal Longitude { get; set; }

    [Required]
    public int Radius { get; set; }

    [Required]
    [MinLength(1)]
    [MaxLength(300)]
    public string Description { get; set; }

    [Required]
    public virtual EventType Type { get; set; }

    [Required] 
    public bool Active { get; set; }

    [Required]
    [Display(Name = "Start date")]
    public DateTime DateFrom { get; set; }

    [Required]
    [Display(Name = "End date")]
    public DateTime DateTo { get; set; }

    [Required]
    public DateTime CreatedOn { get; set; }

    public virtual ICollection<Media> Images { get; set; } = new List<Media>();
}