﻿namespace Domain.Entity.Model.EventManagement
{
    public class EventTypeCount
    {
        public int TypeId { get; set; }
        public int Count { get; set; }
    }
}
