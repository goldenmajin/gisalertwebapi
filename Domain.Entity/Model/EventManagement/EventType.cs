﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entity.Model.EventManagement;

public class EventType
{
    public EventType(EventTypeColor color, string name)
    {
        Color = color;
        Name = name;
    }

    public EventType(){}

    public int Id { get; set; }

    [Required]
    public virtual EventTypeColor Color { get; set; }

    [Required]
    [MaxLength(20)]
    public string Name { get; set; }
}