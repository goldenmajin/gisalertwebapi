﻿using Business.Services.EventManagement.Interfaces;
using Business.Services.EventManagement.Services;
using Business.Services.StatisticsManagement.Interfaces;
using Business.Services.StatisticsManagement.Services;
using Business.Services.UserManagement.Interfaces;
using Business.Services.UserManagement.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Business.Services;

public static class ServiceRegistration
{
    public static void AddDependencies(IServiceCollection services)
    {
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IEventDetailService, EventDetailService>();
        services.AddScoped<IStatisticsService, StatisticsService>();
    }
}