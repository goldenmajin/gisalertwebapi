﻿using AutoMapper;
using Business.DataContracts.EventManagement;
using Domain.Entity.Model.EventManagement;

namespace Business.Services
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Event, EventDetail>().ForMember(dest => dest.ImagePaths,
                act => act.MapFrom(src => src.Images.Select(image => image.FilePath)));
            CreateMap<EventType, EventTypeDetail>().ForMember(dest => dest.Color,
                act => act.MapFrom(src => src.Color.HexCode));
        }
    }
}