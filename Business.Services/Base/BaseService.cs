﻿using Domain.Repository.Base;

namespace Business.Services.Base;

public class BaseService<T> where T : class
{
    public readonly IBaseRepository<T> Repository;

    public BaseService(IBaseRepository<T> repository)
    {
        Repository = repository;
    }
}