﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Business.DataContracts.ApplicationManagement;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace Business.Services.UserManagement.Services;

public static class JwtTokenUtil
{
    public static object? GenerateSecurityToken(IdentityUser user, ApplicationSettings appSettings, IList<string> roles)
    {
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
            {
                new("UserID", user.Id)
            }),
            Expires = DateTime.UtcNow.AddDays(1),
            SigningCredentials =
                new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.JwtSecret)),
                    SecurityAlgorithms.HmacSha256Signature),
            Audience = appSettings.Audience,
            Issuer = appSettings.Issuer
        };

        tokenDescriptor.Subject.AddClaim(new Claim(ClaimTypes.Role, roles.First()));

        var tokenHandler = new JwtSecurityTokenHandler();
        var securityToken = tokenHandler.CreateToken(tokenDescriptor);
        var token = tokenHandler.WriteToken(securityToken);
        return new
        {
            token
        };
    }
}