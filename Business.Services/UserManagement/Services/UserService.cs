﻿using System.ComponentModel.DataAnnotations;
using Business.DataContracts.ApplicationManagement;
using Business.DataContracts.UserManagement;
using Business.Services.UserManagement.Interfaces;
using Domain.Repository.EventManagement.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace Business.Services.UserManagement.Services;

public class UserService: IUserService
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;
    private readonly ApplicationSettings _appSettings;

    public UserService(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager,
        IOptions<ApplicationSettings> appSettings)
    {
        _userManager = userManager;
        _roleManager = roleManager;
        _appSettings = appSettings.Value;
    }

    public async Task<IdentityResult> RegisterUser(RegistrationModel registration)
    {
        var user = new IdentityUser
        {
            UserName = registration.UserName,
            Email = registration.Email
        };

        var role = _roleManager.Roles.SingleOrDefault(x => x.Id == registration.RoleId);

        if (role == null)
        {
            throw new ValidationException("The specified role does not exist");
        }

        var result = await _userManager.CreateAsync(user, registration.Password);

        if (result.Succeeded)
        {
            await _userManager.AddToRolesAsync(user, new[] {role.Name});
        }

        return result;
    }

    public async Task<object?> Login(LoginModel model)
    {
        var user = await _userManager.FindByNameAsync(model.UserName);
        if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
        {
            var roles = await _userManager.GetRolesAsync(user);
            return JwtTokenUtil.GenerateSecurityToken(user, _appSettings, roles);
        }

        return null;
    }

    public async Task<IdentityResult> ChangePassword(string userId, PasswordChangeModel model)
    {
        var user = await _userManager.FindByIdAsync(userId);
        return await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
    }

    public IEnumerable<RoleReference> GetRoles()
    {
        return  _roleManager.Roles.Select(x => new RoleReference
        {
            Id = x.Id,
            Name = x.Name
        });
    }
}