﻿using Business.DataContracts.UserManagement;
using Microsoft.AspNetCore.Identity;

namespace Business.Services.UserManagement.Interfaces;

public interface IUserService
{
    Task<IdentityResult> RegisterUser(RegistrationModel registration);
    Task<object?> Login(LoginModel model);
    IEnumerable<RoleReference> GetRoles();
    Task<IdentityResult> ChangePassword(string userId, PasswordChangeModel model);
}