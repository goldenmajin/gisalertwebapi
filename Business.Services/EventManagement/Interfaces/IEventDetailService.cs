﻿using Business.DataContracts.EventManagement;
using Microsoft.AspNetCore.Http;

namespace Business.Services.EventManagement.Interfaces
{
    public interface IEventDetailService
    {
        int Add(EventAdd add, IEnumerable<IFormFile> files);
        void Delete(int id);
        void Edit(int id, EventSet set);
        EventDetail Get(int id);
        IEnumerable<EventDetail> GetAll();
        IEnumerable<int> CheckCollision(PositionInfo p);
        IEnumerable<EventTypeDetail> GetEventTypes();
    }
}
