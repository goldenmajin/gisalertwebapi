﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Business.DataContracts.EventManagement;
using Business.Services.Base;
using Business.Services.CloudServices.Interfaces;
using Business.Services.EventManagement.Interfaces;
using Business.Services.ExceptionHandling;
using Domain.Entity.Model.EventManagement;
using Domain.Entity.Model.MediaManagement;
using Domain.Repository.EventManagement.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Business.Services.EventManagement.Services
{
    public class EventDetailService : BaseService<Event>, IEventDetailService
    {
        private readonly IMapper _mapper;
        private readonly IEventTypeRepository _eventTypeRepository;
        private readonly IImageUploadService _imageUploadService;

        public EventDetailService(IEventRepository repository, IEventTypeRepository eventTypeRepository, IMapper mapper,
            IImageUploadService imageUploadService) : base(repository)
        {
            _eventTypeRepository = eventTypeRepository;
            _mapper = mapper;
            _imageUploadService = imageUploadService;
        }

        public int Add(EventAdd add, IEnumerable<IFormFile> files)
        {
            if (add.DateFrom >= add.DateTo)
            {
                throw new ValidationException("Starting date of the event cannot be equal or surpass end date");
            }
            var type = _eventTypeRepository.GetById(add.TypeId);
            var imagePaths = files.Select(file => _imageUploadService.UploadToCdn(file)).ToList();

            var evt = new Event(add.Title, add.Latitude, add.Longitude, add.Radius, add.Description, type, add.DateFrom,
                add.DateTo)
            {
                Images = imagePaths.Select(path => new Media(path.SecureUrl.AbsoluteUri)).ToList()
            };

            ModelValidator.Validate(evt);

            Repository.Create(evt);
            Repository.Commit();

            return evt.Id;
        }

        public void Edit(int id, EventSet set)
        {
            var evt = Repository.GetById(id);
            var evtType = _eventTypeRepository.GetById(set.TypeId);

            evt.Title = set.Title;
            evt.Longitude = set.Longitude;
            evt.Latitude = set.Latitude;
            evt.Radius = set.Radius;
            evt.Description = set.Description;
            evt.DateFrom = set.DateFrom;
            evt.DateTo = set.DateTo;
            evt.Type = evtType;

            ModelValidator.Validate(evt);

            Repository.Update(evt);
            Repository.Commit();
        }

        public void Delete(int id)
        {
            var evt = Repository.GetById(id);
            evt.Active = false;
            Repository.Commit();
        }

        public EventDetail Get(int id)
        {
            var evt = Repository.GetById(id);
            return _mapper.Map<Event, EventDetail>(evt);
        }

        public IEnumerable<EventDetail> GetAll()
        {
            var events = ((IEventRepository)Repository).GetEvents().ToList();
            return _mapper.Map<IEnumerable<Event>, IEnumerable<EventDetail>>(events);
        }

        public IEnumerable<EventTypeDetail> GetEventTypes()
        {
            var eventTypes = _eventTypeRepository.GetEventTypes();
            return _mapper.Map<IEnumerable<EventType>, IEnumerable<EventTypeDetail>>(eventTypes);
        }

        public IEnumerable<int> CheckCollision(PositionInfo p)
        {
            List<int> eventsNearby = new List<int>();
            var events = Repository.FindByCondition(x => x.Active);
            events.ToList().ForEach(e =>
            {
                var distance = ComputeHaversineDistance(p.Latitude, p.Longitude, (double)e.Latitude, (double)e.Longitude);

                if (distance <= p.CheckZoneRadius + e.Radius / 1000.0f)
                {
                    eventsNearby.Add(e.Id);
                }
            });
            return eventsNearby;
        }

        private static double ComputeHaversineDistance(double lat1, double lon1, double lat2, double lon2)
        {
            //Multiplied by (Math.PI/180) to convert degrees to radians

            double deltaPhi = (Math.PI / 180) * (lat2 - lat1);
            double deltaLambda = (Math.PI / 180) * (lon2 - lon1);

            var phi1 = (Math.PI / 180) * (lat1);
            var phi2 = (Math.PI / 180) * (lat2);

            double radicand = Math.Pow(Math.Sin(deltaPhi / 2), 2) +
                       Math.Pow(Math.Sin(deltaLambda / 2), 2) *
                       Math.Cos(phi1) * Math.Cos(phi2);

            double earthRadiusInKm = 6371;

            double haversineDistance = 2 * earthRadiusInKm * Math.Asin(Math.Sqrt(radicand));

            return haversineDistance;
        }
    }
}
