﻿using System.ComponentModel.DataAnnotations;
using Castle.Core.Internal;

namespace Business.Services.ExceptionHandling
{
    internal static class ModelValidator
    {
        public static void Validate<T>(T model)
        {
            if (model == null)
            {
                throw new ValidationException(); 
            }
            var results = new List<ValidationResult>();
            Validator.TryValidateObject(model, new ValidationContext(model), results, true);
            if (!results.IsNullOrEmpty())
            {
                var errorMessages = results.Select(res => res.ErrorMessage).ToList();
                var formattedMessage = string.Join(",", errorMessages);
                throw new ValidationException(formattedMessage);
            }
        }
    }
}
