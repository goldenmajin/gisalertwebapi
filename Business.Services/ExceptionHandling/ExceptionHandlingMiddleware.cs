﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using Domain.Repository.Base.DomainExceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Shared.Resources;

namespace Business.Services.ExceptionHandling
{
    public class ExceptionHandlingMiddleware: IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (EntityNotFoundException e)
            {
                await CreateResponse(context, HttpStatusCode.BadRequest, e.Message);
            }
            catch (ValidationException e)
            {
                await CreateResponse(context, HttpStatusCode.BadRequest, e.Message);
            }
            catch (Exception)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(new
                {
                    message = ErrorMessages.InternalError
                }));
            }
        }

        private static async Task CreateResponse(HttpContext context, HttpStatusCode code,  string? message)
        {
            context.Response.StatusCode = (int)code;
            await context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                message
            }));
        }
    }
}
