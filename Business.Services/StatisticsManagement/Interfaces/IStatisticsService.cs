﻿using Business.DataContracts.EventManagement;
using Domain.Entity.Model.UserManagement;

namespace Business.Services.StatisticsManagement.Interfaces
{
    public interface IStatisticsService
    {
        IEnumerable<EventTypeStatistic> GetEventTypeStatistic(DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<UserStatistic> GetUsersStatistic();
    }
}
