﻿using Business.DataContracts.EventManagement;
using Business.Services.StatisticsManagement.Interfaces;
using Domain.Entity.Model.UserManagement;
using Domain.Repository.EventManagement.Interfaces;

namespace Business.Services.StatisticsManagement.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly IEventRepository _eventRepository;
        private readonly IEventTypeRepository _eventTypeRepository;
        private readonly IUserRepository _userRepository;

        public StatisticsService(IEventRepository eventRepository, IEventTypeRepository eventTypeRepository,
            IUserRepository userRepository)
        {
            _eventRepository = eventRepository;
            _eventTypeRepository = eventTypeRepository;
            _userRepository = userRepository;
        }

        public IEnumerable<EventTypeStatistic> GetEventTypeStatistic(DateTime? dateFrom, DateTime? dateTo)
        {

            var eventTypes = _eventTypeRepository.GetEventTypes();
            var eventCount = _eventRepository.GetEventTypesWithCount(dateFrom, dateTo).ToList();

            return eventTypes.Select(x => new EventTypeStatistic
            {
                Name = x.Name,
                Count = eventCount.SingleOrDefault(ec => ec.TypeId == x.Id)?.Count ?? 0,
                Color = x.Color.HexCode
            });
        }

        public IEnumerable<UserStatistic> GetUsersStatistic()
        {
            return _userRepository.GetUserCountByRole();
        }
    }
}
