﻿using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;

namespace Business.Services.CloudServices.Interfaces
{
    public interface IImageUploadService
    {
        ImageUploadResult UploadToCdn(IFormFile file);
    }
}
