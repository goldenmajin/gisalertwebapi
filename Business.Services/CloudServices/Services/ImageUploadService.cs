﻿using Business.Services.CloudServices.Interfaces;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;

namespace Business.Services.CloudServices.Services
{
    public class ImageUploadService: IImageUploadService
    {
        private readonly Cloudinary _cloudinary;

        public ImageUploadService(string name, string apiKey, string secretKey)
        {
            _cloudinary = new Cloudinary(new Account(name, apiKey, secretKey));
            _cloudinary.Api.Secure = true;
        }

        public ImageUploadResult UploadToCdn(IFormFile file)
        {
            var stream = file.OpenReadStream();
            var folder = _cloudinary.CreateFolder(Guid.NewGuid().ToString());

            var imageParams = new ImageUploadParams
            {
                Folder = folder.Path,
                File = new FileDescription(file.FileName, stream),
                Overwrite = true
            };

            return _cloudinary.Upload(imageParams);
        }
    }
}
