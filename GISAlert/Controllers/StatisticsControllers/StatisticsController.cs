﻿using Business.DataContracts.UserManagement;
using Business.Services.StatisticsManagement.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GISAlert.Controllers.StatisticsControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        private readonly IStatisticsService _service;
        public StatisticsController(IStatisticsService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("event/by-type")]
        [Authorize(Roles = UserRoles.Administrator)]
        public IActionResult GetEventTypeStatistic([FromQuery]DateTime? dateFrom, [FromQuery]DateTime? dateTo)
        {
            return Ok(_service.GetEventTypeStatistic(dateFrom, dateTo));
        }

        [HttpGet]
        [Route("users/by-roles")]
        [Authorize(Roles = UserRoles.Administrator)]
        public IActionResult GetUsersStatistic()
        {
            return Ok(_service.GetUsersStatistic());
        }
    }
}
