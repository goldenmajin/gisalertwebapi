﻿using Business.DataContracts.EventManagement;
using Business.DataContracts.UserManagement;
using Business.Services.EventManagement.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GISAlert.Controllers.EventControllers
{
    [Route("api/[controller]")] //api/event
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly IEventDetailService _detailService;
        public EventController(IEventDetailService detailService)
        {
            _detailService = detailService;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetEvents()
        {
            return Ok(_detailService.GetAll());
        }

        [HttpGet]
        [Route("{id:int}")]
        public IActionResult Get(int id)
        {
            return Ok(_detailService.Get(id));
        }

        [HttpGet]
        [Route("types")]
        public IActionResult GetEventTypes()
        {
            return Ok(_detailService.GetEventTypes());
        }

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = UserRoles.Staff)]
        public IActionResult Add([FromForm]IEnumerable<IFormFile> files)
        {
            var formData = HttpContext.Request.Form["add"];
            EventAdd add = JsonConvert.DeserializeObject<EventAdd>(formData);
            return Ok(_detailService.Add(add, files));
        }

        [HttpPut]
        [Route("edit/{id:int}")]
        [Authorize(Roles = UserRoles.Staff)]
        public IActionResult Edit(int id, EventSet detail)
        {
            _detailService.Edit(id, detail);
            return Ok();
        }

        [HttpPatch]
        [Route("delete/{id:int}")]
        [Authorize(Roles = UserRoles.Staff)]
        public IActionResult Delete(int id)
        {
            _detailService.Delete(id);
            return Ok();
        }

        [HttpPost]
        [Route("collision/check")]
        public IActionResult CheckCollision(PositionInfo p)
        {
            return Ok(_detailService.CheckCollision(p));
        }
    }
}