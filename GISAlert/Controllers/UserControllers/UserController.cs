﻿using System.Security.Claims;
using Business.DataContracts.UserManagement;
using Business.Services.UserManagement.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GISAlert.Controllers.UserControllers
{
    [Route("api/[controller]")] //api/user
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("register")]
        [Authorize(Roles = UserRoles.Administrator)]
        public async Task<IActionResult> Register(RegistrationModel model)
        {
            var result = await _userService.RegisterUser(model);
            if (!result.Succeeded)
            {
                return BadRequest(new
                {
                    errors = string.Join(",",result.Errors.Select(e => e.Description))
                });
            }
            return Ok();
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var response = await _userService.Login(model);
            if (response == null)
            {
                return BadRequest(new { message = "Invalid credentials" });
            }
            return Ok(response);
        }

        [HttpGet]
        [Route("roles")]
        public IActionResult GetRoles()
        {
            return Ok(_userService.GetRoles());
        }

        [HttpPut]
        [Route("password/change")]
        [Authorize(Roles = UserRoles.Administrator)]
        public async Task<IActionResult> ChangePassword(PasswordChangeModel model)
        {
            var userId = HttpContext.User.FindFirstValue("UserID");
            var result = await _userService.ChangePassword(userId, model);
            return Ok(result);
        }
    }
}
