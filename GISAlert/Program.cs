using System.Text;
using Business.DataContracts.ApplicationManagement;
using Business.Services;
using Business.Services.CloudServices.Interfaces;
using Business.Services.CloudServices.Services;
using Business.Services.ExceptionHandling;
using Domain.Repository;
using Domain.Repository.Context;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<ApplicationDbContext>(options => 
    options.UseSqlServer(builder.Configuration.GetConnectionString("GISAlertDatabase")));
builder.Services.AddDbContext<AuthorizationDbContext>(options => 
    options.UseSqlServer(builder.Configuration.GetConnectionString("IdentityServer")));
builder.Services.AddAutoMapper(typeof(MappingProfile));
builder.Services.AddIdentity<IdentityUser, IdentityRole>(options =>
    {
        options.User.RequireUniqueEmail = false;
        options.Password.RequireDigit = true;
        options.Password.RequireNonAlphanumeric = true;
        options.Password.RequiredLength = 8;
        options.Password.RequireLowercase = true;
        options.Password.RequireUppercase = true;
    })
    .AddEntityFrameworkStores<AuthorizationDbContext>()
    .AddDefaultTokenProviders();

//Set up Cloudinary CDN
string cloudName = builder.Configuration["CloudinaryAccount:CloudName"];
string apiKey = builder.Configuration["CloudinaryAccount:ApiKey"];
string apiSecret = builder.Configuration["CloudinaryAccount:ApiSecret"];
builder.Services.AddSingleton<IImageUploadService, ImageUploadService>(x => new ImageUploadService(cloudName, apiKey, apiSecret));

//Add exception handling
builder.Services.AddTransient<ExceptionHandlingMiddleware>();

//Services and repositories registrations
RepositoryRegistration.AddDependencies(builder.Services);
ServiceRegistration.AddDependencies(builder.Services);
builder.Services.Configure<ApplicationSettings>(builder.Configuration.GetSection("ApplicationSettings"));
builder.Services.AddCors();

var key = Encoding.UTF8.GetBytes(builder.Configuration["ApplicationSettings:JwtSecret"]);

builder.Services.AddAuthentication(x =>
    {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(x =>
    {
        x.RequireHttpsMetadata = false;
        x.SaveToken = false;
        x.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(key),
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidAudience = builder.Configuration["ApplicationSettings:Audience"],
            ValidIssuer = builder.Configuration["ApplicationSettings:Issuer"],
            ClockSkew = TimeSpan.Zero
        };
    });

var app = builder.Build();

app.UseCors(c => c.AllowAnyHeader()
    .AllowAnyMethod().AllowAnyOrigin());

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseMiddleware<ExceptionHandlingMiddleware>();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
