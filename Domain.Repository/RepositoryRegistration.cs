﻿using Domain.Repository.EventManagement.Interfaces;
using Domain.Repository.EventManagement.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Domain.Repository;

public static class RepositoryRegistration
{
    public static void AddDependencies(IServiceCollection services)
    {
        services.AddScoped<IEventRepository, EventRepository>();
        services.AddScoped<IEventTypeRepository, EventTypeRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
    }
}