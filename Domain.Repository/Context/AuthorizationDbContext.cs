﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Domain.Repository.Context;

public class AuthorizationDbContext : IdentityDbContext<IdentityUser, IdentityRole, string>
{
    public AuthorizationDbContext(DbContextOptions<AuthorizationDbContext> options) : base(options)
    {

    }
}