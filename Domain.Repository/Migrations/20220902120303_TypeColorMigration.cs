﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Domain.Repository.Migrations
{
    public partial class TypeColorMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HexCode",
                table: "EventTypeColors",
                type: "nvarchar(6)",
                maxLength: 6,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HexCode",
                table: "EventTypeColors");
        }
    }
}
