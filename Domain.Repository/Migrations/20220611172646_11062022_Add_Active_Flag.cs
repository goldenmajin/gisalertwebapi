﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Domain.Repository.Migrations
{
    public partial class _11062022_Add_Active_Flag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ExpireDate",
                table: "Events",
                newName: "DateTo");

            migrationBuilder.RenameColumn(
                name: "CreatedOn",
                table: "Events",
                newName: "DateFrom");

            migrationBuilder.AlterColumn<decimal>(
                name: "Longitude",
                table: "Events",
                type: "decimal(10,7)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(9,7)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Latitude",
                table: "Events",
                type: "decimal(10,7)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(9,7)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Events",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)",
                oldMaxLength: 150);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Events",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Events");

            migrationBuilder.RenameColumn(
                name: "DateTo",
                table: "Events",
                newName: "ExpireDate");

            migrationBuilder.RenameColumn(
                name: "DateFrom",
                table: "Events",
                newName: "CreatedOn");

            migrationBuilder.AlterColumn<decimal>(
                name: "Longitude",
                table: "Events",
                type: "decimal(9,7)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(10,7)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Latitude",
                table: "Events",
                type: "decimal(9,7)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(10,7)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Events",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300);
        }
    }
}
