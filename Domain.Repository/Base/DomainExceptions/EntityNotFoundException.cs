﻿using System.Runtime.Serialization;
namespace Domain.Repository.Base.DomainExceptions;

[Serializable]
public class EntityNotFoundException : Exception
{
    public EntityNotFoundException()
    {
    }

    public EntityNotFoundException(string message) : base(message)
    {
    }
    protected EntityNotFoundException(SerializationInfo info,
        StreamingContext context) : base(info, context)
    {
    }
};