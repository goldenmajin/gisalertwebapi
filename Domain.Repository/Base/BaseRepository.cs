﻿using System.Linq.Expressions;
using Domain.Repository.Base.DomainExceptions;
using Domain.Repository.Context;

namespace Domain.Repository.Base;

public class BaseRepository<T> : IBaseRepository<T> where T : class
{
    public readonly ApplicationDbContext _context;

    public BaseRepository(ApplicationDbContext context)
    {
        _context = context;
    }
    public IQueryable<T> GetAll()
    {
        return _context.Set<T>();
    }

    public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
    {
        return _context.Set<T>().Where(expression);
    }

    public void Create(T entity)
    {
        _context.Set<T>().Add(entity);
    }

    public void Update(T entity)
    {
        _context.Set<T>().Update(entity);
    }

    public void Delete(T entity)
    {
        _context.Set<T>().Remove(entity);
    }

    public T GetById(int id)
    {
        var entity = _context.Set<T>().Find(id);
        if (entity == null)
        {
            throw new EntityNotFoundException($"Entity with id {id} could not be found");
        }
        return entity;
    }

    public void Commit()
    {
        _context.SaveChanges();
    }
}