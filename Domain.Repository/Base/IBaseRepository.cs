﻿using System.Linq.Expressions;

namespace Domain.Repository.Base;

public interface IBaseRepository<T>
{
    IQueryable<T> GetAll();
    IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
    void Create(T entity);
    void Update(T entity);
    void Delete(T entity);
    T GetById(int id);
    void Commit();
}