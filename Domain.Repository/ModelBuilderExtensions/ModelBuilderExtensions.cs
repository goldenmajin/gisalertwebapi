﻿using Domain.Entity.Model.EventManagement;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Domain.Repository.ModelBuilderExtensions;

public static class ModelBuilderExtensions
{
    public static void SeedUsers(this ModelBuilder modelBuilder)
    {
        List<IdentityRole> roles = new List<IdentityRole>
        {
            new() { Name = "Moderator", NormalizedName = "Moderator"},
            new() { Name = "Administrator", NormalizedName = "Administrator"}
        };

        modelBuilder.Entity<IdentityRole>().HasData(roles);

        IdentityUser applicationAdmin = new IdentityUser
        {
            UserName = "adminAccount",
            NormalizedUserName = "ADMINACCOUNT",
            Email = "adminAccount@email.com",
            NormalizedEmail = "ADMINACCOUNT@EMAIL.COM"
        };

        modelBuilder.Entity<IdentityUser>().HasData(applicationAdmin);

        var passwordHasher = new PasswordHasher<IdentityUser>();
        applicationAdmin.PasswordHash = passwordHasher.HashPassword(applicationAdmin, "adminAccount@123");

        IdentityUserRole<string> userRole = new IdentityUserRole<string>
        {
            UserId = applicationAdmin.Id,
            RoleId = roles.First(x => x.Name == "Administrator").Id
        };

        modelBuilder.Entity<IdentityUserRole<string>>().HasData(userRole);
    }
}