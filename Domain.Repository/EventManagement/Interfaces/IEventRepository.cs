﻿using Domain.Entity.Model.EventManagement;
using Domain.Repository.Base;

namespace Domain.Repository.EventManagement.Interfaces
{
    public interface IEventRepository: IBaseRepository<Event>
    {
        IEnumerable<Event> GetEvents();
        IEnumerable<EventTypeCount> GetEventTypesWithCount(DateTime? dateFrom, DateTime? dateTo);
    }
}
