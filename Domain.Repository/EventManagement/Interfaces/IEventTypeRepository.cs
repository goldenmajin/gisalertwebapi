﻿using Domain.Entity.Model.EventManagement;
using Domain.Repository.Base;

namespace Domain.Repository.EventManagement.Interfaces;

public interface IEventTypeRepository: IBaseRepository<EventType>
{
    IEnumerable<EventType> GetEventTypes();
}