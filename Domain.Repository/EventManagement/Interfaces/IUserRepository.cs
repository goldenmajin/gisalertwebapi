﻿using Domain.Entity.Model.UserManagement;

namespace Domain.Repository.EventManagement.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<UserStatistic> GetUserCountByRole();
    }
}
