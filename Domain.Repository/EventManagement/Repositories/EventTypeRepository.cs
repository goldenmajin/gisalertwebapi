﻿using Domain.Entity.Model.EventManagement;
using Domain.Repository.Base;
using Domain.Repository.Context;
using Domain.Repository.EventManagement.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Domain.Repository.EventManagement.Repositories;

public class EventTypeRepository:BaseRepository<EventType>, IEventTypeRepository
{
    public EventTypeRepository(ApplicationDbContext context) : base(context)
    {
    }

    public IEnumerable<EventType> GetEventTypes()
    {
        return GetAll().Include(x => x.Color);
    }
}