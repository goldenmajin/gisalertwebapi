﻿using Domain.Entity.Model.UserManagement;
using Domain.Repository.Context;
using Domain.Repository.EventManagement.Interfaces;

namespace Domain.Repository.EventManagement.Repositories
{
    public class UserRepository: IUserRepository
    {
        private readonly AuthorizationDbContext _context;
        public UserRepository(AuthorizationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<UserStatistic> GetUserCountByRole()
        {
            return _context.Users.Join(_context.UserRoles, user => user.Id,
                    userRole => userRole.UserId,
                    (user, userRole) => new {User = user, UserRole = userRole})
                .Join(_context.Roles, q => q.UserRole.RoleId,
                    role => role.Id,
                    (q, role) => new {User = q.User.Id, Role = role.Name})
                .GroupBy(x => x.Role)
                .Select(x => new UserStatistic(x.Key){Count = x.Count()});
        }
    }
}
