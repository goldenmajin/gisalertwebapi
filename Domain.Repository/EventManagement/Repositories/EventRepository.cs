﻿using Domain.Entity.Model.EventManagement;
using Domain.Repository.Base;
using Domain.Repository.Context;
using Domain.Repository.EventManagement.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Domain.Repository.EventManagement.Repositories;

public class EventRepository: BaseRepository<Event>, IEventRepository
{
    public EventRepository(ApplicationDbContext context) : base(context)
    {
    }

    public IEnumerable<Event> GetEvents()
    {
        return FindByCondition(x => x.Active && x.DateFrom < DateTime.Now && DateTime.Now < x.DateTo)
            .Include(x => x.Type).Include(x => x.Images).Include(x => x.Type.Color);
    }

    public IEnumerable<EventTypeCount> GetEventTypesWithCount(DateTime? dateFrom, DateTime? dateTo)
    {
        IQueryable<Event> query = _context.Events;
        
        if (dateFrom != null)
        {
            query = query.Where(x => x.CreatedOn >= dateFrom);
        }

        if (dateTo != null)
        {
            query = query.Where(x => x.CreatedOn <= dateTo);
        }

        return query.GroupBy(x => x.Type.Id).Select(x => new EventTypeCount{ TypeId = x.Key, Count = x.Count()});
    }
}