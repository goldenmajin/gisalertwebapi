﻿namespace Business.DataContracts.EventManagement;

public class EventTypeStatistic
{
    public string? Name { get; set; }
    public int Count { get; set; }
    public string? Color { get; set; }
}