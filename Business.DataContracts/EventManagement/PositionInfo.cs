﻿namespace Business.DataContracts.EventManagement
{
    public class PositionInfo
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double CheckZoneRadius { get; set; }
    }
}
