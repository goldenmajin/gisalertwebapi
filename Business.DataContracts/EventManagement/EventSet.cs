﻿namespace Business.DataContracts.EventManagement;

public class EventSet
{
    public string Title { get; set; } = default!;
    public decimal Longitude { get; set; }
    public decimal Latitude { get; set; }
    public int Radius { get; set; }
    public int TypeId { get; set; }
    public string Description { get; set; } = default!;
    public DateTime DateFrom { get; set; }
    public DateTime DateTo { get; set; }
}