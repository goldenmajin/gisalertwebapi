﻿namespace Business.DataContracts.EventManagement
{
    public class EventTypeDetail
    {
        public int Id { get; set; }
        public string Name { get; set; } = default!;
        public string Color { get; set; } = default!;
    }
}
