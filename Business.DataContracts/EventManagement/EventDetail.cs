﻿namespace Business.DataContracts.EventManagement
{
    public class EventDetail
    {
        public int Id { get; set; }
        public string Title { get; set; } = default!;
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int Radius { get; set; }
        public string Description { get; set; } = default!;
        public EventTypeDetail? Type { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public IEnumerable<string> ImagePaths { get; set; }

        public EventDetail()
        {
            ImagePaths = new List<string>();
        }
    }
}
