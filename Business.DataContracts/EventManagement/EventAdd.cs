﻿namespace Business.DataContracts.EventManagement
{
    public class EventAdd
    {
        public string Title { get; set; } = default!;
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int Radius { get; set; }
        public string Description { get; set; } = default!;
        public int TypeId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
