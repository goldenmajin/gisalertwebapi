﻿namespace Business.DataContracts.ApplicationManagement
{
    public class ApplicationSettings
    {
        public string JwtSecret { get; set; } = default!;
        public string Audience { get; set; } = default!;
        public string Issuer { get; set; } = default!;
    }
}
