﻿namespace Business.DataContracts.UserManagement
{
    public static class UserRoles
    {
        public const string Administrator = "Administrator";
        public const string Moderator = "Moderator";
        public const string Staff = Administrator + "," + Moderator;
    }
}
