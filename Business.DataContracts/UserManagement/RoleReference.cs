﻿namespace Business.DataContracts.UserManagement
{
    public class RoleReference
    {
        public string? Id { get; set; }
        public string? Name { get; set; }
    }
}
